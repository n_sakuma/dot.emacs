
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;      SLIME
;;
;;
;; Git版 - SLIME
;; git://git.boinkor.net/slime.git # 回線が細い
;; https://github.com/technomancy/slime/
;;
;; http://common-lisp.net/project/slime/snapshots/slime-current.tgz
;;
;;  ------------------------------


(setq slime-lisp-implementations
      `((clojure ("/usr/local/bin/clj")) ;; homebrew 版
        (sbcl ("/usr/local/bin/sbcl") :coding-system utf-8-unix))) ;; homebrew 版

;; (setq inferior-lisp-program "/opt/sbcl/bin/sbcl") ;;
;; (setq inferior-lisp-program "/usr/local/bin/clj")

(require 'slime)
(eval-after-load 'slime '(setq slime-protocol-version 'ignore))
(slime-setup '(slime-repl))
;; (slime-setup '(slime-fancy))


;; ;; (SLIME) 閉じ括弧を全て補完する
(global-set-key "\C-cj" 'slime-close-all-parens-in-sexp)

;; ;; ;; カーソル付近にある単語の情報を表示
;; ;; (add-hook 'slime-load-hook (lambda () (require 'slime-autodoc)))

;; ;;  日本語利用
(setq slime-net-coding-system 'utf-8-unix)

(define-key global-map "\C-cs" 'slime)



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;     Clojure
;;
;;
;; http://github.com/technomancy/clojure-mode

;; clojure-code
;; package(ELPA) でインストール

;; swank-clojure
(setq swank-clojure-jar-path "/usr/local/Cellar/clojure/1.2.1/clojure.jar"
      swank-clojure-extra-classpaths (list "/usr/local/Cellar/clojure-contrib/1.2.0/clojure-contrib.jar"))



;;;;;;;;;;;;;;;;;;;;
;;
;;   Common Lisp
;;


(add-to-list 'auto-mode-alist '("\\.l$" . lisp-mode))
(add-to-list 'auto-mode-alist '("\\.cl$" . lisp-mode))
(add-to-list 'auto-mode-alist '("\\.lisp$" . lisp-mode))

(autoload 'paredit-mode "paredit"
  "Minor mode for pseudo-structurally editing Lisp code." t)

;;=======================================================================
;; HyperSpec
;;=======================================================================
;;
;; Ports版 - /opt/local/var/macports/software/lisp-hyperspec/7.0_0/opt/local/share/doc/lisp/HyperSpec-7-0/HyperSpec/

;; (setq hyperspec-path
;;       "/opt/local/var/macports/software/lisp-hyperspec/7.0_0/opt/local/share/doc/lisp/HyperSpec-7-0/HyperSpec/")

;; (setq common-lisp-hyperspec-root (concat "file://" hyperspec-path)
;;       common-lisp-hyperspec-symbol-table (concat hyperspec-path "Data/Map_Sym.txt"))

;; HyperSpecをw3mで見る
;; (defadvice common-lisp-hyperspec
;;   (around hyperspec-lookup-w3m () activate)
;;   (let* (
;;       ;;(window-configuration (other-window-configuration))
;;       (window-configuration (current-window-configuration))
;;          (browse-url-browser-function
;;           `(lambda (url new-window)
;;              (w3m-browse-url url nil)
;;              (let ((hs-map (copy-keymap w3m-mode-map)))
;;                (define-key hs-map (kbd "q")
;;                  (lambda ()
;;                    (interactive)
;;                    (kill-buffer nil)
;;                    (set-window-configuration ,window-configuration)))
;;                (use-local-map hs-map)))))
;;     ad-do-it))

;; (define-key global-map "\C-ch" 'hyperspec-lookup)






;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;     Scheme-mode
;;


;;;;;;;;;;;;;
;; Gauche
;;

(modify-coding-system-alist 'process "gosh" '(utf-8 . utf-8))
;; (setq scheme-program-name "gosh -i")
;; (autoload 'scheme-mode "cmuscheme" "Major mode for Scheme." t)
;; (autoload 'run-scheme "cmuscheme" "Run an inferior Scheme process." t)
(defun scheme-other-window ()
  (setq scheme-program-name "gosh -i")
  "Run scheme on other window"
  (interactive)
  (switch-to-buffer-other-window
   (get-buffer-create "*scheme*"))
  (run-scheme scheme-program-name))

(defun gauche-info ()
  (interactive)
  (switch-to-buffer-other-frame
   (get-buffer-create "*info*"))
  (info "/opt/local/share/info/gauche-refe.info.gz"))

(define-key global-map
  "\C-cg" 'scheme-other-window)
(define-key global-map
  "\C-ci" 'gauche-info)
